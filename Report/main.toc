\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Problem Definition}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Decision Problem}{5}{0}{1}
\beamer@subsectionintoc {1}{2}{Computational Problem}{6}{0}{1}
\beamer@subsectionintoc {1}{3}{Optimisation Problem}{7}{0}{1}
\beamer@sectionintoc {2}{Testing Methodology}{8}{0}{2}
\beamer@sectionintoc {3}{Random Instances Sampling Strategy}{9}{0}{3}
\beamer@sectionintoc {4}{Solutions}{10}{0}{4}
\beamer@subsectionintoc {4}{1}{Exhaustive Search}{11}{0}{4}
\beamer@subsectionintoc {4}{2}{Dynamic Programming}{19}{0}{4}
\beamer@subsectionintoc {4}{3}{Greedy}{26}{0}{4}
\beamer@subsectionintoc {4}{4}{GRASP}{36}{0}{4}
\beamer@subsectionintoc {4}{5}{Iterative Improvement}{49}{0}{4}
\beamer@subsectionintoc {4}{6}{Special cases}{60}{0}{4}
\beamer@sectionintoc {5}{Conclusion and Recommendations}{66}{0}{5}
\beamer@sectionintoc {6}{Reflections}{68}{0}{6}
