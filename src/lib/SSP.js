'use strict'


/**
 * Subset-Sum Problem class
 *  -Represent a single subset-sum problem
 *
 * @class SSP
 */
class SSP {
	constructor(setOptions, t) {
			this.s = this.seed(setOptions.length, setOptions.max)
			this.t = t || this.generateValidTargetFromSet(this.s)

			this.solve = this.solve.bind(this)
	}

	/**
	 * @name seed
	 * @description Returns an array of random integers to use for s.
	 * @param {integer} length - Desired array length
	 * @param {integer} max - Maximum element value
	 * @returns {Array<integer>} - Array of integers
	 *
	 * @memberOf SSP
	 */
	seed(length, max) {
		return Array.from(Array(length), x => Math.floor(Math.random() * max))
	}

	/**
	 * @name generateValidTargetFromSet
	 * @description Sums a random amount of integers from the given set.
	 * Randomly selects indexes to sum,
	 * will not sum the same element more than once.
	 * @param {any} set - Set to generate target from
	 * @returns {integer} - target
	 *
	 * @memberOf SSP
	 */
	generateValidTargetFromSet(set) {
		// Random number of elements to sum.
		const sampleSize = Math.floor(Math.random() * set.length)
		// const indexes = []
		// let index
		// return Array.from(Array(sampleSize), x => {
		// 	// Randomly select an index
		// 	index = Math.floor(Math.random() * (set.length-1))
		// 	// Check if index has already been used.
		// 	while(indexes.find((el, i) => el === index)) {
		// 		index = Math.floor(Math.random() * (set.length-1))
		// 	}
		// 	indexes.push(index)
		// 	return set[index]
		// }).reduce((prev, curr) => prev + curr)

		let copy = Array.from(set)
		let x = 0
		for (let i = 0; i < sampleSize; i++) {
			let index = Math.floor(Math.random() * copy.length)
			x += copy[index]
			copy.splice(index, 1)
		}
		return x
	}

	/**
	 * @name solve
	 * @description Calls given function & returns time taken to complete in ms.
	 * @param {function} method - function to call
	 * @param {boolean} [verbose=false] - (OPTIONAL) Output SSP data to console after completion
	 * @returns
	 *
	 * @memberOf SSP
	 */
	solve(method, verbose = false) {
		const start = Date.now()
		const result = method(this.s, this.t, 10)
		const end = Date.now()
		const runtime = end - start
		if (verbose) {
			console.log(`Successful: ${result}, time-elapsed(ms): ${runtime}`)
			console.log(`set: ${this.s}`)
			console.log(`target: ${this.t}`)
		}
		return {
			result,
			runtime
		}
	}
}

module.exports = SSP
