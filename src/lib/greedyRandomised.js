'use strict'

const greedyRandomised = (S, t) => {
	// Quicksort Descending
	let k = Array.from(S).sort((a, b) => b - a)
	let c = []
	// Get first index <= t
	let total = 0
	let min = k.findIndex(el => el + total <= t)
	do {
		const r  = Math.floor(Math.random() * ((k.length - 1) - min + 1) + min)
		c.push(k[r])
		total += k[r]
		k.splice(r, 1)

		min = k.findIndex(el => el + total <= t)
	} while(min != -1)
	return {
		c,
		d: t - total
	}
}

module.exports = greedyRandomised

// let avg1 = []
// let avg2 = []
// console.log(`TESTING SOLUTION 1`)
// for(let i = 0; i < 100; i++) {
// 	console.log(i)
// 	let p = new SSP({length: 30000, max: 10000})
// 	let start = Date.now()
// 	let res = greedyRandomised(p.s, p.t)
// 	let end = Date.now()
// 	avg1.push(end - start)
// }
// console.log(`TESTING SOLUTION 2`)
// for(let i = 0; i < 10; i++) {
// console.log(i)
// 	let p = new SSP({length: 30000, max: 10000})
// 	let start = Date.now()
// 	let res = y(p.s, p.t)
// 	let end = Date.now()
// 	avg2.push(end - start)
// }
// console.log(` AVG for greedyRandomised: ${getTotal(avg1) / 100}`)
// console.log(` AVG for y: ${getTotal(avg2) / 100}`)
