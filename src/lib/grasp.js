'use strict'
const greedy = require('./greedy')
const greedyRandomised = require('./greedyRandomised')

function GRASP(S, t, k) {
	let i = 0
	let bestCandidate = null
	let currTotal = 0
	while (i < k && currTotal !== t) {
		const greedyCandidate = greedyRandomised(S, t)
		if (i === 0) {
			bestCandidate = greedyCandidate
			currTotal = t - greedyCandidate.d
		}
		const graspCandidate = greedyLocalSearch(S, t, greedyCandidate)

		if (scoreCandidate(graspCandidate, t) > scoreCandidate(greedyCandidate, t)) {
			bestCandidate = Object.assign({}, graspCandidate)
			currTotal = t - graspCandidate.d
		}
		i++
	}
	return bestCandidate || {c: [], d: 0}
}

// Polynomial approximation
function greedyLocalSearch(S, t, c) {
	// Construct Set of unused elements
	const neighborhood = constructUnusedElements(S, c.c) //O(n)

	// Remove a random Candidate
	const newCandidate = { c: Array.from(c.c), d: c.d}
	const indexToRemove = Math.floor(Math.random() * newCandidate.c.length)
	let total = (t - c.d) - newCandidate.c[indexToRemove]
	newCandidate.c.splice(indexToRemove, 1)
	let i = 0
	// Replace element with next largest while total <= t
	do {
		if (neighborhood[i] + total <= t) { // O(n)
			newCandidate.c.push(neighborhood[i])
			total += neighborhood[i]
		}
		i++
	} while (i < neighborhood.length)

	return {
		c: newCandidate.c ,
		d: t - total
	}
}

// Higher score is better
function scoreCandidate(c, t) {
	return t - c.d
}
/**
 * Returns Sum value of array.
 *
 * @param {any} c
 * @returns
 */
function getTotal(c) {
	c = c || []
	return c.reduce((acc, curr) => acc + curr, 0)
}


/**
 * Returns a Subset of S whose, elements are not in the candidate solutions
 *
 * @param {any} S - Original Set
 * @param {any} c - Candidate Solution set
 */
function constructUnusedElements(S, c) {
	const neighborhood = Array.from(S)
	c.forEach(el => {
		neighborhood.splice(neighborhood.findIndex(x => x === el), 1) //O(|c|n)
	})
	return neighborhood
}

module.exports = GRASP
