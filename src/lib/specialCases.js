'use strict'

/**
 * @name validateSSP
 * @description Validates SSP for known limitations
 * @param {any} s - Set S
 * @param {any} t - Target integer t
 * @returns {boolean} - Returns false for invalid SSP.
 */
function validateSSP(s, t) {
	// 1. if t > total sum of  s
	if (t > s.reduce((acc, curr) => acc + curr, 0)) return false

	// 2. t < smallest element in s
	if (t < Math.min.apply(null, s)) return false

	return true
}


/**
 * @name reduceSet
 * @description Returns the subset of S with the values <= t.
 * @param {integer} s - Set S
 * @param {integer} t - target integer t
 */
function reduceSet(s, t) {
	return s.filter(el => el <= t) //O(n)
}

module.exports = {
	validateSSP,
	reduceSet
}
