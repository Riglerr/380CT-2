'use strict'

/**
 * @name exhaustiveSearch
 * @description Exhaustive Search - Enumerates every subset of s
 * 	halts when a subset has a summative value of t.
 * @returns <Boolean> - Idicative of if a subset could be found with a sum value of t
 */
module.exports = function exhaustive(s, t, verbose = false) {
	const total = Math.pow(2, s.length)
	let mask = 0
	let result
	for (mask; mask < total; mask++) {
		result = []
		let i = s.length - 1
		do {
			if ((mask & (1 << i)) !== 0) {
				result.push(s[i])
			}
		} while (i--)
		if (result.reduce((acc, curr)  => acc + curr, 0) === t) return true
	}
	return false
}

const s = [1, 2, 2, 4, 5, 9]
const t  = 15

module.exports(s, t)
