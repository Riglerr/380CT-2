'use strict'

function greedy(s, t) {
	let a = Array.from(s) // Copy S
	let total = 0
	while(a.reduce((acc, curr) => curr + total <= t ? acc + 1 : acc,0) > 0) { // O(n)
		let currMaxIndex = null
		let i = 0
		for (i; i < a.length; i++) { // O(n)
			if (a[i] >= (a[currMaxIndex] || 0) && a[i] + total <= t) {
				currMaxIndex = i
			}
		}
		if (currMaxIndex) {
			total += a[currMaxIndex] || 0
			a.splice(currMaxIndex, 1)
		}
	}
	const r = t - total
	return r
}

function greedyWithSort(s, t ) {
	let total = 0
	let c = []
	s.sort((a, b) => b - a ) // Quicksort Descending O(nlog n)
	for(let i = 0; i < s.length; i++) { // O(n)
		if (s[i] + total <= t) {
			c.push(s[i])
			total += s[i]
		}
	}
	return {
		c,
		d: t - total
	}
}

module.exports = greedyWithSort
