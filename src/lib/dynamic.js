'use strict'

module.exports = function dynamic(s, t) {
	// Initialize Matrix m (first column (0) is always true)
	let m = Array.from(Array(s.length), x => {
		return Array.from(Array(t + 1), (el, i) => {
			return i === 0 ? true : null
		})
	})
	// m = [][]
	for(let i = 0; i < s.length; i++) { // O(t)
		for(let j = 1; j < t + 1; j++) { // O(n)
			if (i === 0) {
				m[i][j] = j === s[i]
			} else {
				m[i][j] = m[i - 1][j] || m[i-1][j-s[i]] ? true : false
			}
		}
	}

	return m[s.length - 1][t]
}

function drawTable(s, r) {
	for (let i = -1; i < s.length; i++) {
		if (i === -1) {
			let topRow = ""
			for(let j = 0; j < r[0].length; j ++) {
				topRow = topRow.concat(`	${j}`)
			}
			console.log(topRow)
		} else {
			let line = `${s[i]}`
			for (let j = 0; j < r[0].length; j++) {
				line = line.concat(`	${r[i][j]}`)
			}
			console.log(line)
		}
	}
}

// const s = [2,2,4,5,9]
// drawTable(s, module.exports(s, 15))
// console.log( module.exports(s, 12))
