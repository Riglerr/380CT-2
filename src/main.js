'use strict'

const fs = require('fs')
const path = require('path')
const SSP = require('./lib/SSP')
const exhaustiveSearch = require('./lib/exhaustive')
const dynamic = require('./lib/dynamic')
const greedy = require('./lib/greedy')
const greedyRandomised = require('./lib/greedyRandomised')
const grasp = require('./lib/grasp')
/**
 * Solves a new SSP %sample_size% times.
 * Averages the time elapsed for all executions.
 * @returns {Array<Object>} - Array of average execution results for each set length.
 * @returns {Array<Object.x>} - Length of set S
 * @returns {Array<Object.y>} - Average execution time in ms
 * @returns {Array<Object.min>} - Lowest execution time in ms
 * @returns {Array<Object.max>} -  Highest execution time in ms
 */
function decision(method, setLengths, sampleSize, maxElementValue, writeToFile = false, verbose = false, latex = false) {
	let retVal = []
	for (let i = 0; i < setLengths.length; i++) {
		let setLength = lengths[i]
		let max = 0
		let min = Number.MAX_VALUE

		const setResult = Array.from(Array(sampleSize), (x, index) => {
			verbose ? console.log(`Processing: ${setLength}  ${index + 1}/${sampleSize}`) : null

			let iterationResult = new SSP({ length: setLength, max: maxElementValue }).solve(method)
			max = iterationResult.runtime > max ? iterationResult.runtime : max
			min = iterationResult.runtime < min ? iterationResult.runtime : min
			return iterationResult
		})

		retVal.push({
			x: setLength,
			y: setResult.reduce((acc, curr) => acc + curr.runtime) / sampleSize,
			min,
			max
		})
	}

	if (!writeToFile) return retVal
	const fileData = {
		sampleSize,
		maxElementValue,
		data: retVal
	}
	const stringData = JSON.stringify(fileData)
	fs.writeFile(path.join(__dirname, '/out', `${method.name}.json`), stringData, (err) => {
		if (err) {
			console.log(err)
		}
		console.log('done.')
	})
}

function optimisation(method, setLengths, sampleSize, maxElementValue, writeToFile = false, verbose = false, latex) {
	let retVal = []
	for (let i = 0; i < setLengths.length; i++) {
		let setLength = lengths[i]
		let max = 0
		let min = Number.MAX_VALUE
		let dMin = Number.MAX_VALUE
		let dMax = 0
		const setResult = Array.from(Array(sampleSize), (x, index) => {
			verbose && ((index + 1) % 5 === 0) ? console.log(`Processing: ${setLength}  ${index + 1}/${sampleSize}`) : null

			let iterationResult = new SSP({ length: setLength, max: maxElementValue }).solve(method)
			max = iterationResult.runtime > max ? iterationResult.runtime : max
			min = iterationResult.runtime < min ? iterationResult.runtime : min

			dMax = iterationResult.result.d > dMax ? iterationResult.result.d : dMax
			dMin = iterationResult.result.d < dMin ? iterationResult.result.d : dMin
			return iterationResult
		})

		retVal.push({
			setLength,
			runtime: {
				avg: setResult.reduce((acc, curr) => acc + curr.runtime, 0) / sampleSize,
				lowest: min,
				highest: max
			},
			distance: {
				avg: Math.round(setResult.reduce((acc, curr) => acc + curr.result.d, 0) / sampleSize),
				lowest: dMin,
				highest: dMax
			}
		})
	}

	if (!writeToFile) return retVal
	const fileData = {
		sampleSize,
		maxElementValue,
		data: retVal
	}
	let distanceTable, timeTable
	if (latex) {
		distanceTable = generateTabularString(fileData, true)
		timeTable = generateTabularString(fileData)
	}
	const logpath = path.join(__dirname, '/out', `${method.name}.json`)
	const stringData = JSON.stringify(fileData, null, '\t').concat(distanceTable, timeTable)
	console.log(`Writing output to: ${logpath}`)
	fs.writeFile(logpath, stringData, (err) => {
		if (err) {
			console.log(err)
		}
		console.log('done.')
	})
}

function generateTabularString(res, distance = false) {
	let r = ''
	if (distance) {
		r = r.concat('\\resizebox{\\columnwidth}{!}{%\\begin{tabular}{| c | c | c | c |}\\hline Set Length (n) & Avg. Distance from $t$ & Lowest Distance from $t$& Highest Distance from $t$ \\\\ \\hline')
		res.data.forEach(el => {
		r = r.concat(`${el.setLength} & ${el.distance.avg} & ${el.distance.lowest} & ${el.distance.highest} \\\\ \\hline`)
	})
	} else {
		r = r.concat('\\resizebox{\\columnwidth}{!}{%\\begin{tabular}{| c | c | c | c |}\\hline Set Length (n) & Avg. Time-Elapsed & Lowest Time-Elapsed& Highest Time-Elapsed \\\\ \\hline')
		res.data.forEach(el => {
		r = r.concat(`${el.setLength} & ${el.runtime.avg} & ${el.runtime.lowest} & ${el.runtime.highest} \\\\ \\hline`)
	})
	}


	r = r.concat('\\end{tabular}}')
	return r
}

let data = []
const max_num = 10000
const sample_size = 50
//let lengths = [10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,180,190,200,
// 210,220,230,240,250,260,270,280,290,300,310,320,330,340,350,360,370,380,490,500, 1000, 1500, 2000, 5000, 8000, 10000]
let lengths = [10, 20, 30, 40, 50, 100, 200, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000, 15000, 20000, 25000, 30000]
//let lengths = [30000]
// decision(exhaustiveSearch, lengths, sample_size, max_num, true, true)
// decision(dynamic, lengths, sample_size, max_num, true, true)

 //optimisation(greedy, lengths, sample_size, max_num, true, true, true)
optimisation(grasp, lengths, sample_size, max_num, true, true, true)
//optimisation(greedyRandomised, lengths, sample_size, max_num, true, true, true)


