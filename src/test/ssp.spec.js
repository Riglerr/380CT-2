'use strict'

const {expect} = require('chai')
const SSP = require('../lib/SSP')


describe('SSP', () => {
	describe('seeding s', () => {
		it('should return an array of length equal to length parameter', () => {
			const p = new SSP({length: 10, max: 1000})
			const seedResult = p.seed(100, 1000)
			expect(seedResult).to.be.an('array').of.length(100)
		})

		it('should return an array where each element is a random integer below the max parameter', () => {
			const p = new SSP({length: 10, max: 1000})
			const seedResult = p.seed(100, 1000)
			expect(seedResult).to.be.an('array').of.length(100)
			seedResult.forEach((el) => expect(el).to.be.below(1000))
		})
	})

	describe('solving', () => {
		it('should return integer respresenting time-taken to execute method function parameter', () => {
			const p = new SSP({length: 10, max: 1000})
			const ms = p.solve(() => {
				const end = Date.now() + 1500
				while(Date.now() < end) {
					//nil
				}
			})
			expect(ms).to.be.gte(1500)
		})
	})
})